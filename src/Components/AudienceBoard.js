import React, { Component } from "react";
import { Menu, Button } from "antd";
import { Icon } from "antd-v3";
import "../App.css";

const { SubMenu } = Menu;

class AudienceBoard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      collapsed: false,
    };
  }

  toggleCollapsed = () => {
    this.setState({
      collapsed: !this.state.collapsed,
    });
  };
  render() {
    const mydata = this.props.formObject;

    return (
      <>
        <div style={{ width: 256 }}>
          <Button
            type="primary"
            onClick={this.toggleCollapsed}
            style={{ marginBottom: 10, backgroundColor: "#00BFFF" }}
          >
            <Icon type={this.state.collapsed ? "menu-unfold" : "menu-fold"} />
            <span style={{ color: "black" }}>Audience Board</span>
          </Button>
          {/* {console.log(Object.entries(mydata))} */}
          {Object.entries(mydata).map((each) => (
            <div key={each[0]}>
              <Menu
                key="sub1"
                defaultSelectedKeys={["1"]}
                defaultOpenKeys={["sub1"]}
                mode="inline"
                inlineCollapsed={this.state.collapsed}
                style={{ backgroundColor: "#4863A0" }}
              >
                <SubMenu
                  key="sub1"
                  title={
                    <span>
                      <Icon type="user" style={{ color: "white" }} />
                      <span style={{ color: "white" }}>{each[1].size}</span>
                    </span>
                  }
                >
                  <Menu.Item key="1">
                    <div>
                      <button
                        style={{
                          width: `${100}px`,
                          height: `${20}vh`,
                          borderRadius: `${2}px`,
                          marginTop: 2,
                          backgroundColor: "skyblue",
                        }}
                      >
                        {each[1].audienceCategory}
                      </button>
                    </div>
                  </Menu.Item>
                  <Menu.Item key="2">
                    <div>
                      <Icon type="appstore" style={{ color: "black" }} />
                      Strategic Imperative
                    </div>
                  </Menu.Item>
                  <Menu.Item key="3">
                    <div className="prefillBtn">
                      <Icon type="deployment-unit" style={{ color: "black" }} />{" "}
                      Strategic Action
                      <Icon
                        type="eye"
                        htmltype="submit"
                        onClick={() => this.props.getPrefillId(each[0])}
                      />
                    </div>
                  </Menu.Item>
                </SubMenu>
              </Menu>
            </div>
          ))}
        </div>
      </>
    );
  }
}

export default AudienceBoard;
