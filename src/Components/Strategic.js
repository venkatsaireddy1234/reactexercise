import React, { Component } from "react";
import "../App.css";
import AudienceBoard from "./AudienceBoard";
import FormComponent from "./FormComponent";

class Strategic extends Component {
  constructor(props) {
    super(props);
    this.state = {
      formData: {},
      id: "",
    };
  }

  saveFormData = (id, data) => {
    console.log(id);
    this.setState(
      { formData: { ...this.state.formData, [id]: data }, id:""},
      () => console.log(this.state.formData)
    );
  };

  getPrefillId = (id) => {
    this.setState(
      {
        id: id,
      },
      () => console.log(this.state.id)
    );
  };

  render() {
    return (
      <div className="parent">
        <FormComponent
          saveFormData={this.saveFormData}
          formData={this.state.formData[this.state.id]}
          formID={this.state.id}
        />
        <AudienceBoard
          formObject={this.state.formData}
          getPrefillId={this.getPrefillId}
        />
      </div>
    );
  }
}

export default Strategic;
