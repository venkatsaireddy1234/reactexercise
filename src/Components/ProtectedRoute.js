import React, { Component } from "react";
import { Route, Redirect } from "react-router-dom";

class ProtectedRoute extends Component {
  render() {
    const { component: Component } = this.props;
    return (
      <Route
        render={() =>
          localStorage.getItem("loginDetails") ? (
            Object.keys(JSON.parse(localStorage.getItem("loginDetails")))
              .length ? (
              <Component />
            ) : (
              <Redirect to="/AboutUs" />
            )
          ) : (
            <Redirect to="/AboutUs" />
          )
        }
      />
    );
  }
}

export default ProtectedRoute;
