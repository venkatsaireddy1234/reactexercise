/* eslint-disable no-restricted-globals */
import React, { Component } from "react";
import indegenlogo from "../Images/Indegene.png";
import "bootstrap/dist/css/bootstrap.min.css";
import { Button, Checkbox, Form, Input } from "antd";
import { Link } from "react-router-dom";
import {  LoginConsumer } from "./LoginContext/Index";


class AboutUs extends Component {
  render() {
    // console.log(localStorage.getItem('loginDetails', 'abouts us page'));
    return (
      <>
        <LoginConsumer>
          {(props) => {
            const { username,password,formOnChange, handleLink,authenticated } = props;
            // console.log(username,password,"consumer");
            return (
              <div className="wholeContainer">
                <div className="leftContainer">
                  <div>
                    <img src={indegenlogo} alt="Indegenelogo" />
                    <div>
                      <h1 style={{ color: "skyblue" }}>AboutUs</h1>
                      <p style={{ color: "lightskyblue", fontSize: `${25}px` }}>
                        We are a technology-led healthcare solutions provider
                      </p>
                    </div>
                  </div>
                </div>
                <div className="login">
                  <Form
                    name="basic"
                    labelCol={{
                      span: 8,
                    }}
                    wrapperCol={{
                      span: 16,
                    }}
                    initialValues={{
                      remember: true,
                    }}
                    onValuesChange={formOnChange}
                    autoComplete="off"
                  >
                    <Form.Item
                      label="Username"
                      name="username"
                      rules={[
                        {
                          required: true,
                          message: "Please input your username!",
                        },
                        {
                          type: "email",
                          message: "Please enter a valid Email Address",
                        },
                      ]}
                      hasFeedback
                    >
                      <Input />
                    </Form.Item>

                    <Form.Item
                      label="Password"
                      name="password"
                      rules={[
                        {
                          required: true,
                          message: "Please input your password!",
                        },
                        { min: 6 },
                      ]}
                      hasFeedback
                    >
                      <Input.Password />
                    </Form.Item>

                    <Form.Item
                      name="remember"
                      valuePropName="checked"
                      wrapperCol={{
                        offset: 8,
                        span: 16,
                      }}
                    >
                      <Checkbox>Remember me</Checkbox>
                    </Form.Item>

                    <Form.Item
                      wrapperCol={{
                        offset: 8,
                        span: 16,
                      }}
                    >
                      <Link to="/Dashboard" onClick={()=>handleLink(username,password,authenticated)}>
                        <Button type="primary" htmlType="submit">
                          Login
                        </Button>
                      </Link>
                    </Form.Item>
                  </Form>
                </div>
              </div>
            );
          }}
        </LoginConsumer>
      </>
    );
  }
}

export default AboutUs;
