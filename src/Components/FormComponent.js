import React, { Component } from "react";
import { Tabs } from "antd";
import { Form, Select, Input, Space } from "antd";
import { Button } from "antd";
import { v4 as uuidv4 } from "uuid";
import { PlusOutlined } from "@ant-design/icons";

const { TabPane } = Tabs;
const { TextArea } = Input;

class FormComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      audienceCategory: "",
      audeinceName: "",
      bussinessPriority: "",
      size: "",
      strategicImperative: "",
      currentBehaviour: "",
      driversandBarriers: "",
      desiredImpact: "",
      strategicaction: "",
    };
  }

  formRef = React.createRef();

  resetState = () => {
    this.setState(
      {
        audienceCategory: "",
        audeinceName: "",
        bussinessPriority: "",
        size: "",
        strategicImperative: "",
        currentBehaviour: "",
        driversandBarriers: "",
        desiredImpact: "",
        strategicaction: "",
      },
      () => this.formRef.current.resetFields()
    );
  };

  checkFormData = () => {
    if (Object.values(this.state).some((element) => element === "")) {
      return false;
    }
    return true;
  };

  save = () => {
    if (this.checkFormData()) {
      // console.log("hi");
      const id = uuidv4();
      this.props.saveFormData(this.props.formID || id, { ...this.state });
      this.resetState();
    }
  };

  formOnChange = (values, allvalues) => {
    // console.log("triggeronFinish", values);
    this.setState(
      (prevState) => {
        return {
          ...prevState,
          ...values,
        };
      },
      () => this.save()
    );
  };

  onFill = () => {
    this.formRef.current.setFieldsValue(this.props.formData);
    // this.setState(this.props.formData);
    this.formOnChange();
  };

  componentDidUpdate(prevProps) {
    // console.log(prevProps);
    if ( prevProps.formID !== this.props.formID) {
      console.log("hiii");
      this.onFill();
    }
  }

  render() {
    return (
      <>
        <div>
          <Tabs
            style={{
              //   width: `${140}vh`,
              borderRadius: `${10}px`,
              margin: `${1}px`,
            }}
            type="card"
          >
            <TabPane
              className="Strategicplanning"
              tab="Strategic planning"
              key="1"
            >
              Target Audience Analysis
              <hr />
              <Form
                ref={this.formRef}
                name="basic"
                labelCol={{
                  span: 20,
                  offset: -5,
                }}
                wrapperCol={{
                  span: 15,
                  offset: 7,
                }}
                onFinish={this.formOnChange}
                autoComplete="off"
              >
                <div className="targetAudienceAnalysis">
                  <div className="targetAudience">
                    <h5 style={{ margin: `${5}px` }}>Target Audience</h5>
                    <div>
                      <div className="align">
                        <Form.Item
                          label="Audience Category"
                          name="audienceCategory"
                          rules={[
                            {
                              required: true,
                              message: "It can't be empty!",
                            },
                          ]}
                          hasFeedback
                        >
                          <Input
                            placeholder="Enter Audience Category"
                            value={this.state.audienceCategory}
                          />
                        </Form.Item>

                        <Form.Item
                          label="Audeince Name(Persona)"
                          name="audeinceName"
                          rules={[
                            {
                              required: true,
                              message: "Please Select any of one!",
                            },
                          ]}
                        >
                          <Select placeholder="Select">
                            <Select.Option value="Coltina">
                              Coltina
                            </Select.Option>
                            <Select.Option value="Colina">Colina</Select.Option>
                            <Select.Option value="Coltna">Coltna</Select.Option>
                          </Select>
                        </Form.Item>
                        <Form.Item
                          label="Bussiness Priority"
                          name="bussinessPriority"
                          rules={[
                            {
                              required: false,
                              message: "Please Enter Your Business Priority!",
                            },
                          ]}
                          hasFeedback
                        >
                          <Input
                            placeholder="Enter Your Business Priority!"
                            value={this.state.bussinessPriority}
                          />
                        </Form.Item>
                        <Form.Item
                          label="size"
                          name="size"
                          rules={[
                            {
                              required: false,
                              message: "Please Enter size",
                            },
                          ]}
                          hasFeedback
                        >
                          <Input
                            placeholder="Enter size"
                            value={this.state.size}
                          />
                        </Form.Item>
                      </div>
                    </div>
                  </div>
                  <div className="strategicImperative">
                    <h5 style={{ margin: `${5}px` }}>Brand Direction</h5>
                    <div className="brand">
                      <Form.Item
                        label="Strategic Imperative"
                        name="strategicImperative"
                        rules={[
                          {
                            required: true,
                            message: "Please Enter size",
                            placeholder: "Enter Strategic Imperative",
                          },
                        ]}
                        hasFeedback
                      >
                        <TextArea
                          rows={8}
                          style={{ width: 600 }}
                          placeholder="Enter Strategic Imperative"
                          value={this.state.strategicImperative}
                        />
                      </Form.Item>
                    </div>
                  </div>
                  <div className="behavorialAnalysis">
                    <div className="behavorialAnalysis">
                      <h5 style={{ margin: `${5}px` }}>BehavorialAnalysis</h5>
                      <div className="behavorialAnalysiscontentDefault">
                        <Form.Item
                          label="Current Behaviour"
                          name="currentBehaviour"
                          rules={[
                            {
                              required: true,
                              message: "Please Enter Current Behaviour",
                            },
                          ]}
                          hasFeedback
                        >
                          <TextArea
                            rows={5}
                            style={{ width: 250 }}
                            placeholder="Enter Current Behaviour"
                          />
                        </Form.Item>
                        <Form.Item
                          label="Drivers and Barriers"
                          name="driversandBarriers"
                          rules={[
                            {
                              required: true,
                              message: "Please Enter Drivers and Barriers",
                            },
                          ]}
                          hasFeedback
                        >
                          <TextArea
                            rows={5}
                            style={{ width: 250 }}
                            placeholder="Enter Drivers and Barriers"
                            value={this.state.driversandBarriers}
                          />
                        </Form.Item>
                        <Form.Item
                          label="Desired Imapct"
                          name="desiredImpact"
                          rules={[
                            {
                              required: true,
                              message: "Please Enter Desired Impact",
                            },
                          ]}
                          hasFeedback
                        >
                          <TextArea
                            rows={5}
                            style={{ width: 250 }}
                            placeholder="Enter Desired Impact"
                            value={this.state.desiredImpact}
                          />
                        </Form.Item>
                        <Form.Item
                          label="Strategic Action"
                          name="strategicaction"
                          rules={[
                            {
                              required: true,
                              message: "Please Enter Strategic Action",
                            },
                          ]}
                          hasFeedback
                        >
                          <TextArea
                            rows={5}
                            style={{ width: 250 }}
                            placeholder="Enter Strategic Action"
                            value={this.state.strategicaction}
                          />
                        </Form.Item>
                      </div>
                    </div>
                    <div className="addNew">
                      <Form.List name="users">
                        {(fields, { add, remove }) => (
                          <>
                            {fields.map(({ key, name, ...restField }) => (
                              <div>
                                <div>
                                  <div className="behavorialAnalysiscontent">
                                    <Space
                                      key={0}
                                      style={{
                                        display: "flex",
                                        marginBottom: 5,
                                      }}
                                      align="baseline"
                                    >
                                      <Form.Item
                                        key={5}
                                        {...restField}
                                        label="Current Behaviour"
                                       
                                        name={[name, "Current Behaviour"]}
                                        rules={[
                                          {
                                            required: true,
                                            message:
                                              "Please Enter Current Behaviour",
                                          },
                                        ]}
                                        hasFeedback
                                      >
                                        <TextArea
                                          rows={5}
                                          style={{ width: 250 }}
                                          placeholder="Enter Current Behaviour"
                                          value={this.state.users}
                                        />
                                      </Form.Item>
                                      <Form.Item
                                        key={6}
                                        {...restField}
                                        label="Drivers and Barriers"
                                        
                                        name={[name, "Drivers and Behaviour"]}
                                        rules={[
                                          {
                                            required: true,
                                            message:
                                              "Please Enter Drivers and Barriers",
                                          },
                                        ]}
                                        hasFeedback
                                      >
                                        <TextArea
                                          rows={5}
                                          style={{ width: 250 }}
                                          placeholder="Enter Drivers and Barriers"
                                    
                                          
                                        />
                                      </Form.Item>
                                      <Form.Item
                                        key={7}
                                        {...restField}
                                        label="Desired Imapct"
                                        
                                        name={[name, "Desired Impact"]}
                                        rules={[
                                          {
                                            required: true,
                                            message:
                                              "Please Enter Desired Impact",
                                          },
                                        ]}
                                        hasFeedback
                                      >
                                        <TextArea
                                          rows={5}
                                          style={{ width: 250 }}
                                          placeholder="Enter Desired Impact"
                                          
                                          
                                        />
                                      </Form.Item>
                                      <Form.Item
                                        key={8}
                                        {...restField}
                                        label="Strategic Action"
                                        
                                        name={[name, "strategicaction"]}
                                        rules={[
                                          {
                                            required: true,
                                            message:
                                              "Please Enter Strategic Action",
                                          },
                                        ]}
                                        hasFeedback
                                      >
                                        <TextArea
                                          rows={5}
                                          style={{ width: 250 }}
                                          placeholder="Enter Strategic Action"
                                          
                                        
                                        />
                                      </Form.Item>
                                    </Space>
                                    <div className="dnagerbtn">
                                      <Button
                                        type="danger"
                                        onClick={() => remove(name)}

                                        // block
                                        //   icon={<MinusCircleOutlined/>}
                                      >
                                        Delete
                                      </Button>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            ))}
                            <Form.Item>
                              <Button
                                style={{ color: "skyblue" }}
                                onClick={() => add()}
                                block
                                icon={<PlusOutlined />}
                              >
                                Add New
                              </Button>

                              {/* <MinusCircleOutlined onClick={() => remove(name)} /> */}
                            </Form.Item>

                            <div></div>
                          </>
                        )}
                      </Form.List>
                      
                    </div>
                  </div>

                  <Form.Item
                  
                  >
                    <Button type="primary" htmlType="submit">
                      Save
                    </Button>
                  </Form.Item>
                </div>
              </Form>
            </TabPane>
            <TabPane tab="Campaign Plan" key="2">
              Campaign Plan
            </TabPane>
          </Tabs>
        </div>
      </>
    );
  }
}

export default FormComponent;
