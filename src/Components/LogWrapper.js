import React, { Component } from "react";
import "../App.css";

const Context = React.createContext("");
export const LogContext = Context;

class LogWrapper extends Component {
  render() {
    const { children, ...propsfromComponent } = this.props;
    return (
      <LogContext.Consumer>
        {(consumedPropsFromParent) => {
          const combinedProps = { ...consumedPropsFromParent, ...propsfromComponent };
          return (
            <LogContext.Provider value={combinedProps}>
              <button
                onClick={() =>
                  console.log(Object.values(combinedProps), "Button Clicked")
                }
              >
                {propsfromComponent.button}
              </button>
              {children}  
            </LogContext.Provider>
          );
        }}
      </LogContext.Consumer>
    );
  }
}
//
export default LogWrapper;
