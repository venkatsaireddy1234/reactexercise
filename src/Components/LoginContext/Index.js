import React from "react";

const LoginContext = React.createContext({
  username: "",
  password: "",
  authenticated: true,
});

const LoginConsumer = LoginContext.Consumer;

class LoginOperations extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: "",
      authenticated: true,
    };
  }

  handleClear = () => {
    this.setState({
      username: "",
      password: "",
      authenticated: true,
    });
  };

  formOnChange = (values, oldValues) => {
    console.log("Success:", values, oldValues);
    this.setState({
      username: oldValues.username,
      password: oldValues.password,
    });
  };

  handleLink = (username, password) => {
    localStorage.setItem(
      "loginDetails",
      username && password ? JSON.stringify(this.state) : JSON.stringify({})
    );
    console.log(this.state);
  };

  componentDidMount = () => {
    window.addEventListener("storage", (e) => {
      this.setState(
        {
          authenticated: false,
        },
        () => {
          this.setState({
            username: "",
            password: "",
            authenticated: true,
          });
          // localStorage.clear("loginDetails");
        }
      );
    });
  };

  render() {
    console.log(this.state, "state");
    return (
      <LoginContext.Provider
        value={{
          username: this.state.username,
          password: this.state.password,
          authenticated: this.state.authenticated,
          formOnChange: this.formOnChange,
          handleLink: this.handleLink,
          handleClear: this.handleClear,
        }}
      >
        {this.props.children}
      </LoginContext.Provider>
    );
  }
}

export default LoginOperations;

export { LoginContext, LoginConsumer };
