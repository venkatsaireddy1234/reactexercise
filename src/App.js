import React, { Component } from "react";
import "./App.css";
import AboutUs from "./Components/AboutUs";
import Dashboard from "./Components/Dashboard";
import { BrowserRouter, Switch, Route, Redirect } from "react-router-dom";
import ProtectedRoute from "./Components/ProtectedRoute";
import LoginOperations from "./Components/LoginContext/Index";
// import FormHandling from "./Components/FormContext/Context";

class App extends Component {
  render() {
    return (
      <>
        <LoginOperations>
          <BrowserRouter>
            <Switch>
              <Route exact path="/">
                {localStorage.getItem("loginDetails") === null ? 
                  <Redirect to="/AboutUs" />
                 : Object.keys(localStorage.getItem("loginDetails")).length ||
                  localStorage.getItem("loginDetails") === null ? 
                  <Redirect to="/Dashboard" />
                 : 
                  <Redirect to="/AboutUs" />
                }
              </Route>
              <Route exact path="/AboutUs" component={AboutUs} />
              {/* <FormHandling> */}
              <ProtectedRoute path="/Dashboard" component={Dashboard} />
              {/* </FormHandling> */}
            </Switch>
          </BrowserRouter>
        </LoginOperations>
      </>
    );
  }
}

export default App;
